package com.chichemeinc.mobile.smstest;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.chichemeinc.mobile.smstest.util.NetworkUtil;

public class MainActivity extends AppCompatActivity {

    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.chichemeinc.mobile.smstest.R.layout.activity_main);

        this.activity = this;

        Typeface fontAwesome = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
        final TextView labelRetornoIcone = (TextView) findViewById(R.id.labelRetornoIcone);
        labelRetornoIcone.setTypeface(fontAwesome);

        final TextView labelRetorno = (TextView) findViewById(R.id.labelRetorno);

        Button buttonSolicitarCodigo = (Button) findViewById(R.id.buttonSolicitarCodigo);
        buttonSolicitarCodigo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (NetworkUtil.verifyConnectivity(activity)) {

                }

            }
        });

        Button buttonEnviarCodigo = (Button) findViewById(R.id.buttonEnviarCodigo);
        buttonEnviarCodigo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (NetworkUtil.verifyConnectivity(activity)) {

                    boolean valido = false;

                    if (valido) {
                        labelRetornoIcone.setText(getResources().getText(R.string.icon_valid));
                        labelRetornoIcone.setTextColor(Color.parseColor("#274E13"));
                        labelRetorno.setText("Código válido");
                    } else {
                        labelRetornoIcone.setText(getResources().getText(R.string.icon_invalid));
                        labelRetornoIcone.setTextColor(Color.parseColor("#660000"));
                        labelRetorno.setText("Código inválido");
                    }
                }

            }
        });
    }

}
