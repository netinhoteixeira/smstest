package com.chichemeinc.mobile.smstest.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.chichemeinc.mobile.smstest.util.NetworkUtil;

import org.apache.http.client.ClientProtocolException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by Francisco Ernesto Teixeira on 02/07/15.
 */
public class SolicitarCodigoTask extends AsyncTask<Void, Void, String> {

    private final Activity activity;
    private ProgressDialog progressDialog;
    private String numeroTelefone;

    public SolicitarCodigoTask(Activity activity, String numeroTelefone) {
        this.activity = activity;
        this.progressDialog = new ProgressDialog(activity);
        this.numeroTelefone = numeroTelefone;
    }

    protected void onPreExecute() {
        this.progressDialog.setMessage("Solicitando código...");
        this.progressDialog.show();
        this.progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
                SolicitarCodigoTask.this.cancel(true);
            }
        });
    }

    @Override
    protected String doInBackground(Void... params) {
        InputStream inputStream = null;
        String result = "";

        String url = "http://yoururlhere.com/?numeroTelefone=" + numeroTelefone;

        try {
            inputStream = NetworkUtil.openHttpConnection(url);
        } catch (UnsupportedEncodingException e1) {
            Log.e("UnsupportedEncodingEx", e1.toString());
            e1.printStackTrace();
        } catch (ClientProtocolException e2) {
            Log.e("ClientProtocolEx", e2.toString());
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            Log.e("IllegalStateEx", e3.toString());
            e3.printStackTrace();
        } catch (IOException e4) {
            Log.e("IOEx", e4.toString());
            e4.printStackTrace();
        }

        // Convert response to string using String Builder
        try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
            StringBuilder sBuilder = new StringBuilder();

            String line;
            while ((line = bReader.readLine()) != null) {
                sBuilder.append(line + "\n");
            }

            inputStream.close();
            result = sBuilder.toString();

        } catch (Exception ex) {
            Log.e("StrBuild & BuffReader", "Error converting result " + ex.toString());
        }

        return result;
    }

    protected void onPostExecute(String result) {
        Toast.makeText(activity, " Solicitação enviada ", Toast.LENGTH_LONG).show();


    }

}